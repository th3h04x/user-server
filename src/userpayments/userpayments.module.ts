import { Module } from '@nestjs/common';
import { UserpaymentsService } from './userpayments.service';
import { UserpaymentsController } from './userpayments.controller';

@Module({
  providers: [UserpaymentsService],
  controllers: [UserpaymentsController]
})
export class UserpaymentsModule {}
