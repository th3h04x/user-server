import { Controller, Get, Post } from '@nestjs/common';
import { UserpaymentsService } from './userpayments.service';

@Controller('userpayments')
export class UserpaymentsController {
    constructor (
        private readonly userPaymentsService : UserpaymentsService
    ){}

    @Get('unpaid')
    async unpaid_bills(){

    }

    @Post('instantiatePayment')
    async instantiatePayment(){}

    @Post('madePayment')
    async madePayment(){}

    @Get('/')
    async paid_bills(){}
    
}
