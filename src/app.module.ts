import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ChargerModule } from './charger/charger.module';
import { UserpaymentsModule } from './userpayments/userpayments.module';
import { FeedbacksModule } from './feedbacks/feedbacks.module';

@Module({
  imports: [UserModule, ChargerModule, UserpaymentsModule, FeedbacksModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
