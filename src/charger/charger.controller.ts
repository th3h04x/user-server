import { Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { ChargerService } from './charger.service';

@Controller('charger')
export class ChargerController {
    
    constructor(
        private readonly chargerService : ChargerService
    ){}

    @Patch('/chargerDesiredState')
    async chargerDesiredState(){}

    @Get('/removeChargerFromUser/:chargerId')
    async removeChargerFromUser(
        @Param() params 
    ){}

    @Post('/nearestChargers')
    async nearestChargers(){}

    @Get('/check/:id')
    async getChargerDetails(){}

}
