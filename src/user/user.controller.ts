import { Controller, Get, Post } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService : UserService
    ){}

    @Post('loginWithGoogle')
    async loginWithGoogle(){}

    @Post('signupWithGoogle')
    async signupWithGoogle(){}

    @Post('loginWithMobile')
    async loginWithMobile(){}

    @Post('verifyMobileAuthOTP')
    async verifyMobileAuthOTP(){}

    @Post('/updateUser')
    async updateUser(){}

    @Get('me')
    async myDetails(){}

    @Post('authFirebaseMobile')
    async authFirebaseMobile(){}

}
