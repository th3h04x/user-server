import { SchemaFactory } from "@nestjs/mongoose";
import { UserPayment } from "./UserPayment.models";
import * as moment from 'moment'

export const UserPaymentSchema = SchemaFactory.createForClass(UserPayment)

UserPaymentSchema.pre<UserPayment>('save',function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
}
)