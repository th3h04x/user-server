import { Prop , Schema } from "@nestjs/mongoose";
import { Manager } from "../manager/Manager.models";
import { User } from "../user/User.models";
import * as mongoose from 'mongoose'
import * as moment from 'moment'
import {Date, Document} from 'mongoose'

@Schema({
    timestamps: true
})
export class Feedbacks extends Document{
    @Prop({
        type:String,
        required:[true , "review or issue detail is mandatory"]
    })
    details : string

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "users",
        default:null
    })
    userId : User

    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: "managers",
        default:null
    })
    managerId : Manager

    @Prop({
        type: Date,
        default: moment().toDate()
    })
    createdAt : any

    @Prop({
        type: String,
        required: [true , "source is required"]
    })
    source : string

    @Prop({
        type: String,
        enum: ["feedback", "issue"],
        default: "feedback",
        immutable: true
    })
    type : string

    @Prop({
        type:String,
        default:"Not Processed"
    })
    status : string

}
