import {SchemaFactory} from '@nestjs/mongoose'
import moment from 'moment'
import { Manager } from './Manager.models'
import * as bcrypt from 'bcryptjs'

export const ManagerSchema = SchemaFactory.createForClass(Manager)

ManagerSchema.pre<Manager>('save', function(next : Function){
    if(this.isNew){
        this.createdAt = moment().toDate()
    }
    next()
})

ManagerSchema.statics.checkPassword = function(password, encryptedPassword) {
    return bcrypt.compare(password, encryptedPassword)
}

ManagerSchema.statics.hashPassword = function(password) {
    return bcrypt.hash(password, 10)
}